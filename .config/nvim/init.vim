set exrc " Custom init.vim in the root directory of a project can be loaded
set relativenumber
set number "set nohlsearch " Removes the highlighting after searching
set hidden " current buffer can be put into background
set noerrorbells
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set wrap
set ignorecase " In search
set smartcase " Ignore case unless relevent
set termguicolors " Comment out if using wal
set noswapfile
set nobackup
set undodir=~/.config/nvim/undodir
set undofile
set incsearch
set scrolloff=8
set noshowmode " don't show which mode disabled for PowerLine
set completeopt=menuone,noinsert,noselect
set signcolumn=yes
set colorcolumn=80

set mouse=a
set clipboard=unnamed
set background=dark
set autoread " detect when a file is changed
set wrapmargin=8 " wrap lines when coming within n characters from side
set linebreak " set soft wrapping
set showbreak=↪
"set autoindent " automatically set indent of new line
set ttyfast " faster redrawing
set diffopt+=vertical,iwhite,internal,algorithm:patience,hiddenoff
set laststatus=2 " show the status line all the time
set so=7 " set 7 lines to the cursors - when moving vertical
set wildmenu " enhanced command line completion
set showcmd " show incomplete commands
set wildmode=list:longest " complete files like a shell
set shell=$SHELL
set cmdheight=1 " command bar height
set title " set terminal title
set showmatch " show matching braces
set mat=2 " how many tenths of a second to blink
set updatetime=50
set shortmess+=c
set guifont="fira-code":h10
set timeoutlen=1000
set ttimeoutlen=5
set spell

call plug#begin('~/.vim/plugged')
Plug 'gruvbox-community/gruvbox'
Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-gitgutter'
Plug 'valloric/youcompleteme'
Plug 'preservim/nerdtree'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
"Plug 'dylanaraps/wal.vim'
Plug 'vimwiki/vimwiki'
Plug 'github/copilot.vim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'Pocco81/AutoSave.nvim'
Plug 'ervandew/supertab'
call plug#end()

colorscheme gruvbox
"colorscheme wal

let mapleader = " "

" NERDTree
"nmap <F2> :NERDTreeToggle<CR>
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" Make Y behave like the other capital letters
nnoremap Y y$

" Telescope:
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" Using Lua functions
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

" Vim Wiki
let g:vimwiki_list = [{'path': '~/git/notes/', 'syntax': 'markdown', 'ext': '.md'}]

au FileType vimwiki setlocal shiftwidth=4 tabstop=4 noexpandtab

set nocompatible
filetype plugin on
syntax on

" colorizer
lua require'colorizer'.setup()

" autosave
lua << EOF
local autosave = require("autosave")

autosave.setup(
    {
        enabled = true,
        execution_message = "AutoSave: saved at " .. vim.fn.strftime("%H:%M:%S"),
        events = {"InsertLeave", "TextChanged"},
        conditions = {
            exists = true,
            filename_is_not = {},
            filetype_is_not = {},
            modifiable = true
        },
        write_all_buffers = false,
        on_off_commands = true,
        clean_command_line_interval = 0,
        debounce_delay = 135
    }
)
EOF

