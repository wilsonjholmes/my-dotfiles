# my-dotfiles

```bash
# Update system
sudo apt update
sudo apt upgrade

# Install needed programs
sudo apt install -y curl git zsh

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Setup dotfiles bare repo thingy
git clone --separate-git-dir=$HOME/.my-dotfiles git@gitlab.com:wilsonjholmes/my-dotfiles.git my-dotfiles-tmp
rsync --recursive --verbose --exclude '.git' my-dotfiles-tmp/ $HOME/
rm --recursive my-dotfiles-tmp
git --git-dir=$HOME/.my-dotfiles/ --work-tree=$HOME config status.showUntrackedFiles no
git --git-dir=$HOME/.my-dotfiles/ --work-tree=$HOME remote set-url origin git@gitlab.com:wilsonjholmes/my-dotfiles.git

# Get oh-my-zsh plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-history-substring-search.git ~/.oh-my-zsh/custom/plugins/zsh-history-substring-search
git clone https://github.com/zsh-users/zsh-completions.git ~/.oh-my-zsh/custom/plugins/zsh-completions

# Check if focal or bionic
. /etc/os-release
if [ $VERSION_ID == 18.04 ]; then
	git --git-dir=$HOME/.my-dotfiles/ --work-tree=$HOME checkout bionic
elif [ $VERSION_ID == 20.04 ]; then
	git --git-dir=$HOME/.my-dotfiles/ --work-tree=$HOME checkout focal
fi
source ~/.zshrc
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
cargo install exa
source ~/.zshrc

# Clone src git repos
git clone https://github.com/Kethku/neovide.git ~/src/neovide
git clone https://github.com/openscad/openscad.git ~/src/openscad
git clone https://github.com/Mayccoll/Gogh.git ~/src/gogh

# Create "Default" profile in gnome-terminal
# Now run the gogh script for pretty terminal colors
bash ~/src/Gogh/gogh.sh
# Now select the color theme in gnome-terminal settings
```

## Setup
```sh
git init --bare $HOME/.my-dotfiles
echo "alias mydotfiles='git --git-dir=$HOME/.my-dotfiles/ --work-tree=$HOME'" >> ~/.zshrc
source ~/.zshrc
mydotfiles remote add origin git@gitlab.com:wilsonjholmes/my-dotfiles.git
```

## Replication
```sh
git clone --separate-git-dir=$HOME/.my-dotfiles git@gitlab.com:wilsonjholmes/my-dotfiles.git my-dotfiles-tmp
rsync --recursive --verbose --exclude '.git' my-dotfiles-tmp/ $HOME/
rm --recursive my-dotfiles-tmp
```

## Configuration
```sh
mydotfiles config status.showUntrackedFiles no
mydotfiles remote set-url origin git@gitlab.com:wilsonjholmes/my-dotfiles.git
```

## Usage
```sh
mydotfiles status
mydotfiles add .gitconfig
mydotfiles commit -m 'Add gitconfig'
mydotfiles push
```
